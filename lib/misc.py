import errno
import os
import os.path
import random
import re
import socket  # ,wrench....
import subprocess
import sys
import time
import yaml
import lib
import string
import cpuinfo

cpu_info = cpuinfo.get_cpu_info()

nested = "hypervisor" in cpu_info['flags']

if "vendor_id" in cpu_info:
    vendor = cpu_info["vendor_id"]
elif "vendor_id_raw" in cpu_info:
    vendor = cpu_info["vendor_id_raw"]
else:
    vendor = "unknown"


def random_string(len):
    return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(len))

def get_prefix():
    ourlocation = os.path.realpath(__file__)
    ourdir = os.path.dirname(ourlocation)
    return os.path.realpath(ourdir + "/../")

def check_nested():
    return nested

def get_cpu_vendor():
    if vendor == "GenuineIntel":
        return "intel"

    if vendor == "AuthenticAMD":
        return "amd"

    print("Warning: unknown cpu vendor: " + vendor + " defaulting to intel")
    return "intel"


################################################################
def write_file(path, value):
    with open(path, "w") as file:
        file.write(value)

def read_file(path):
    with open(path, "r") as file:
        return file.read()

def read_file_ident(path, ident):

    output = ""
    with open(path, "r") as file:
        line = file.readline()
        while line:
            output += ident + line
            line = file.readline()

        return output


def wait_file(filepath, timeout_sec):
    tries = 0
    while not os.path.exists(filepath):
        if timeout_sec > 0 and tries > timeout_sec:
            return False
        tries += 1
        time.sleep(1)
    return True

def print_file(filepath):
    with open(filepath, "r") as file:
        print(file.read())
        
def load_yaml(path):

    try:
        with open(path) as f:
                return yaml.load(f, Loader = yaml.SafeLoader) or {}
    except yaml.scanner.ScannerError as e:
        raise lib.UserError("Error parsing the configuration file\n" + str(e))

################################################################
_ids = {}
def gen_id(prefix = ""):
    r = _ids[prefix] = _ids.get(prefix, -1) + 1
    return prefix + str(r)

#############################################################################################################################
def daemonize(logfile):

    child = os.fork()

    sys.stdout.flush()

    if child != 0:
        return child

    os.setsid()
    sys.stdout.flush()
    sys.stdin.flush()
    sys.stderr.flush()

    dummy_out = os.open(logfile, os.O_CREAT | os.O_RDWR)  # standard input (0)
    dummy_stdin = os.open("/dev/null", os.O_RDWR)

    os.dup2(dummy_stdin, 0)  # standard input
    os.dup2(dummy_out, 1)  # standard output (1)
    os.dup2(dummy_out, 2)  # standard error (2)
    return 0

#############################################################################################################################

comment_re = re.compile("#[^\n]*", re.RegexFlag.MULTILINE)
join_re = re.compile("\\\\\n\s*", re.RegexFlag.MULTILINE)
comma_re = re.compile(",\s*\n\s*", re.RegexFlag.MULTILINE)
strip_unknown_env_vars = re.compile('\$[A-Za-z_][A-Za-z0-9_]*')
comma_end = re.compile(",\s*$", re.RegexFlag.MULTILINE)

def expand_basic(str):
    #TODO: fix this clusterfuck
    str = comment_re.sub("", str)
    str = join_re.sub("", str)
    str = comma_re.sub(",", str)
    str = os.path.expandvars(str.strip())
    str = strip_unknown_env_vars.sub("", str)
    str = comma_re.sub(r',\n', str)
    str = comma_end.sub('', str)
    return str

#############################################################################################################################
def expand_multiline(commands):

    retval = []
    if commands == None:
        return retval
    
    commands = expand_basic(commands)
    
    for raw_command in commands.split("\n"):
        raw_command = raw_command.strip()
        if raw_command != "":
            retval.append(raw_command)

    return retval
