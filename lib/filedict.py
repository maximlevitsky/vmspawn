import os
import os.path
import yaml
import lib

class FileDict():
    def __init__(self, filename):
        self.filename = filename
        self.load()

    #############################################################
    def set(self, key, value):
        self.dict[key] = value

    #############################################################
    def get(self, key = None, default_value = None):
        if key == None:
            return self.dict
        else:
            return self.dict.get(key, default_value)

    #############################################################
    def clear(self, keep_keys = None):

        if keep_keys == None:
            self.dict.clear()
            return

        new_dict = {}

        for key in self.dict:
            if key in keep_keys:
                new_dict[key] = self.dict[key]

        self.dict = new_dict

    #############################################################
    def print(self):
        for item, value in self.dict.items():
            print(str(item) + ":  " + str(value))
        print("")

    #############################################################

    def load(self):
        self.dict = {}
        if os.path.isfile(self.filename):
            self.dict = lib.load_yaml(self.filename)

    #############################################################

    def save(self):
        try:
            with open(self.filename, "w") as f:
                yaml.dump(self.dict, f)
                f.flush()
        except FileNotFoundError:
            pass
