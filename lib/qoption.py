
# Represents a single qemu option on command line
import re
import lib

comma_pattern = re.compile(r'(,){2,}')

class QOption(object):

    def __init__(self, cmdline):

        self._args_list = []
        self._args_set = {}
        self._option = None
        self._first_arg_impl_name = None

        self._parse_cmdline(cmdline)

    ##########################################################################
    def has_arg(self, arg):
        return arg in self._args_set.keys()

    def get_arg(self, arg, default = None):
        if self.has_arg(arg):
            return self._args_set[arg]["value"]
        return default

    def set_arg(self, arg, value, override):
        if self.has_arg(arg):
            if override:
                self._args_set[arg]["value"] = value
        else:
            item = { "name": arg, 'value': value  }
            self._args_list.append(item)
            self._args_set[arg] = item

    def get_option(self):
        return self._option

    ##########################################################################
    def _parse_cmdline(self, cmdline):
        cmdline = cmdline.strip()

        cmdline = re.sub(comma_pattern, ',', cmdline)
        parts = cmdline.split(maxsplit = 1)
        raw_option = parts[0]

        # check for the dash and remove it
        if raw_option[0] != '-':
            raise lib.UserError("Qemu argument '" + cmdline + "' doesn't start with - (dash)")
        if len(raw_option) == 1:
            raise lib.UserError("Bad qemu argument '-'")

        self._option = raw_option[1:]
        self._type = "qemu:" + self._option

        # table of implicit argument names
        if self._option == "smp":
            self._first_arg_impl_name = "cpus"
        elif self._option == "device" or self._option == "netdev" or self._option == "object":
            self._first_arg_impl_name = "type"

        # just argument without sub-options
        if len(parts) == 1:
            return

        if len(parts) != 2:
            raise lib.UserError("Qemu argument '" + cmdline + "' has stray spaces")

        for raw_arg in parts[1].split(","):
            if '=' in raw_arg:
                (arg, value) = raw_arg.split("=")
            else:
                if len(self._args_list) == 0 and self._first_arg_impl_name != None:
                    # convert first implicit arg to full name
                    arg = self._first_arg_impl_name
                    value = raw_arg
                else:
                    # treat arguments without value as boolean parameters
                    arg = raw_arg
                    value = True

            item = { "name" : arg, 'value' : value  }
            self._args_list.append(item)

            if not arg in self._args_set:
                self._args_set[arg] = item
            else:
                raise lib.UserError("Wierd, argument " + arg + " is repeat ed. Does qemu support such cases?")

    ##########################################################################
    def construct_cmdline(self):
        arg_list = []

        arguments = "-" + self._option

        for arg in self._args_list:
            arg_name = arg['name']
            arg_value = arg['value']

            # add-on pseudo arguments that are not for qemu
            if arg_name[0] == "~":
                continue

            if  (type(arg_value) == type(True) and arg_value == False) or arg_value == None:
                continue
            elif (type(arg_value) == type(True) and arg_value == True):
                arg_list.append(arg_name)
            elif len(arg_list) == 0 and arg_name == self._first_arg_impl_name:
                # convert implicit first arg back to its implicit form
                arg_list.append(arg_value)
            else:
                arg_list.append(arg_name + "=" + str(arg_value))

        if len(arg_list):
            arguments += " " + ",".join(arg_list)

        return arguments
