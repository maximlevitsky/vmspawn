# system imports
import sys


class UserError(Exception):
    pass


class NoResouceLeft(Exception):
    pass

class NotImplemented(Exception):
    pass

class RuntimeError(Exception):
    pass


#########################################################
def exception_handler(exctype, value, tb):
    if exctype is UserError:
        print ("User Error: " + str(value))
        exit(1)
    if exctype is RuntimeError:
        print ("Runtime Error: " + str(value))
        exit(1)
    else:
        sys.__excepthook__(exctype, value, tb)


sys.excepthook = exception_handler
