#!/usr/bin/env python3

import os
import core
import lib
import argparse
import socket
import getpass
import shutil
import subprocess
import setproctitle
import signal

parser = argparse.ArgumentParser(add_help = False)
parser.add_argument("--directory", default = os.getcwd())
parser.add_argument("--verbose", nargs = '?', default = 0, const = 1, type = int)
parser.add_argument("--nouserscripts", help = "Don't run user scrpipts", action = "store_true")

############################################################################################################

def usage():
    print(
    """
    vm [--directory DIR] [--verbose N] [@host] command"

      command can be:
      start - start a virtual machine
      stop - (force) stop a running virtual machine
      status - show status of a running virtual machine
      env - get various vm settings
      edit - edit config.yaml file

      ping - wait for the VM to respond to ping
      ssh - open a ssh connection to a running virtual machine (and/or upload ssh keys)
      tty - open VM's serial tty
      mon - connect to monitors

      sshfs - mount/umount the vm filesystem over ssh fs
      scp - run scp to copy files over ssh from/to the VM

      s0/s3/s5 - power managment commands

      reset - force reset the vm
      kill - force kill the VM

      <any user script placed in .scripts directory and/or ~/.vmspawn/scripts"

    """)
    exit(0)

args, rest_of_cmdline = parser.parse_known_args()

################################################################################################
# parse args, pick args that we keep
#################################################################################################

TARGET_HOSTNAME = None

COMMAND = None
COMMAND_DIR = None
PASSTHROUGH_ARGS = []

for arg in rest_of_cmdline:
    if arg[0] == "@":
        if len(arg) > 1:
            TARGET_HOSTNAME = arg[1:]
        else:
            TARGET_HOSTNAME = socket.getfqdn()
        continue

    elif arg[0] != "-" and COMMAND == None:
        COMMAND = arg
        continue
    else:
        PASSTHROUGH_ARGS.append(arg)

if args.verbose:
    PASSTHROUGH_ARGS.append("--verbose")
    PASSTHROUGH_ARGS.append(str(args.verbose))

if COMMAND == None :
    usage()

#################################################################################################
# select VM directory
#################################################################################################

if (os.path.exists(args.directory + "/.vmlink")):
    args.directory = lib.read_file(args.directory + "/.vmlink").strip()

if args.verbose:
    print(socket.getfqdn() + ": Changing current directory to " + args.directory)

os.chdir(args.directory)

#################################################################################################
# select which command to run and its settings
#################################################################################################

LOCAL_CMD = False
DAEMON_CMD = False
ROOT_CMD = False

METADATA = {}
RUNCONFIG = {}

cmd_dirs = [
    os.path.realpath(os.path.dirname(__file__) + "/cmds/"),
]

if args.nouserscripts == False:
    cmd_dirs = \
        [
            os.getcwd() + "/.scripts/",
            os.getenv("HOME") + "/.vmspawn/scripts"
        ] + cmd_dirs


for dir in cmd_dirs:
    if os.path.exists(dir + "/" + COMMAND):
        COMMAND_DIR = dir
        break
    
if COMMAND_DIR == None:
    print("Error: unrecognized command " + COMMAND)
    exit(1)

if os.path.exists(COMMAND_DIR + "/.metadata.yaml"):
    METADATA = lib.load_yaml(COMMAND_DIR + "/.metadata.yaml")


if COMMAND in METADATA:
    LOCAL_CMD = METADATA[COMMAND].get("local", LOCAL_CMD)
    DAEMON_CMD = METADATA[COMMAND].get("daemon", DAEMON_CMD)
    ROOT_CMD = METADATA[COMMAND].get("root", ROOT_CMD)

if os.path.exists(os.getcwd() + "/.runstatus.yaml"):
    RUNCONFIG = lib.FileDict(os.getcwd() + "/.runstatus.yaml")

#################################################################################################
# select host to run the command on
#################################################################################################

proc_title = "vm " + COMMAND

if RUNCONFIG.get("running", False) == False and not DAEMON_CMD:
    LOCAL_CMD = True

if not LOCAL_CMD:
    if not TARGET_HOSTNAME:
        if RUNCONFIG.get("running", False) == True:
            TARGET_HOSTNAME = RUNCONFIG.get("hostname", socket.getfqdn())
        else:
            TARGET_HOSTNAME = RUNCONFIG.get("last_hostname", socket.getfqdn())

if TARGET_HOSTNAME == socket.getfqdn():
    LOCAL_CMD = True

#################################################################################################
# final command preparations
#################################################################################################

if not LOCAL_CMD:
    SSH = ["ssh", "-tt", TARGET_HOSTNAME]

    if not args.verbose:
        SSH += ["-o", "LogLevel=QUIET" ]

    if DAEMON_CMD:
        # this is used to avoid systemD terminating the command when it exits on a remote host
        # quite a clever way to pollute the applicatons with systemD crap
        # TODO: make this optional
        SSH += ["--", "systemd-run", "--user", "-tdq"]

    PASSTHROUGH_ARGS = map(lambda arg: '"' + arg + '"', PASSTHROUGH_ARGS)
    FINAL_CMD = SSH + ["--", "vm", COMMAND, "--directory", os.getcwd(), "@" ] + [ " ".join(PASSTHROUGH_ARGS) ]

    proc_title += "@" + TARGET_HOSTNAME
else:

    if "SUDO_UID" in os.environ:
        os.environ["USER_ID"] = str(os.environ["SUDO_UID"])
        os.environ["USER_NAME"] = os.environ["SUDO_USER"]
    else:
        os.environ["USER_ID"] = str(os.getuid())
        os.environ["USER_NAME"] = getpass.getuser()


    os.environ["HOSTNAME"] = socket.getfqdn()

    SUDO_PREFIX = []
    if os.getuid() != 0 and ROOT_CMD:
        SUDO_PREFIX = ["sudo", "-E", "--preserve-env=HOME,PATH"]


    FINAL_CMD = SUDO_PREFIX + [COMMAND_DIR + "/" + COMMAND] + PASSTHROUGH_ARGS

#################################################################################################
# run the final command
#################################################################################################

if args.verbose:
    print(socket.getfqdn() + ": Running [ " + ", ".join(FINAL_CMD) + " ]")

setproctitle.setproctitle(proc_title)

#ignore CTLR+C in this process but not in the child
signal.signal(signal.SIGINT, signal.SIG_IGN)

exit(subprocess.run([shutil.which(FINAL_CMD[0])] + FINAL_CMD[1:], 
                    shell = False, 
                    preexec_fn = lambda: signal.signal(signal.SIGINT, signal.SIG_DFL)
).returncode)

