import ast
import glob
import os
import random
import re
import shlex
import signal
import string
import subprocess
import sys
import textwrap
import time
import psutil
import yaml
import launchers
import lib
import core
import devices
import socket

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Standard devices
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class SerialPortDev(devices.Device):
    def __init__(self, vm, socketpath, logfile, wait):
        nowait = "" if wait else ",wait=off"
        super().__init__(vm, "serial_stdio")

        self.add_subdevice(devices.QemuCmdDevice(self.vm, "chr", "-chardev socket,path=" + socketpath + ",id=internal_serial0_chardev,server=on,logfile=" + logfile + nowait))
        self.add_subdevice(devices.QemuCmdDevice(self.vm, "isa_serial", "-device isa-serial,chardev=internal_serial0_chardev,index=0"))

class DebugCon(devices.Device):
    def __init__(self, vm, filepath, port):
        super().__init__(vm, "debugcon_" + str(port))
        self.add_subdevice(devices.QemuCmdDevice(self.vm, "chr", "-chardev file,path=" + filepath + ",id=internal_debugcon0_chardev"))
        self.add_subdevice(devices.QemuCmdDevice(self.vm, "isa_debug", "-device isa-debugcon,chardev=internal_debugcon0_chardev,iobase=" + str(port)))


class HmpMonitor(devices.Device):
    def __init__(self, vm, socketpath):
        super().__init__(vm, "hmp_monitor_socket")
        self.add_subdevice(devices.QemuCmdDevice(self.vm, "chr", "-chardev socket,path=" + socketpath + ",id=internal_hmp_monitor_socket_chardev,server=on,wait=off"))
        self.add_subdevice(devices.QemuCmdDevice(self.vm, "mon", "-mon chardev=internal_hmp_monitor_socket_chardev,mode=readline"))

class QmpMonitor(devices.Device):
    def __init__(self, vm, socketpath):
        super().__init__(vm, "qmp_monitor_socket")

        self.add_subdevice(devices.QemuCmdDevice(self.vm, "chr", "-chardev socket,path=" + socketpath + ",id=internal_qmp_monitor_socket_chardev,server=on,wait=off"))
        self.add_subdevice(devices.QemuCmdDevice(self.vm, "mon", "-mon chardev=internal_qmp_monitor_socket_chardev,mode=control"))

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# This clas represents a VM
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class VM:
    def __init__(self, vmdir, verbose = 0, master = False, settings = None, extra_qemu_cmdline = None):

        # init settings
        self.master = master
        self.verbose = int(verbose)
        self.default_cmdline_switches = {}

        # initialize various directories we will use
        self.vmdir = os.path.realpath(vmdir)
        self.vmname = os.path.basename(self.vmdir)

        # load runconfig and setup socket location
        self.runconfig = lib.FileDict(self.vmdir + "/.runstatus.yaml")

        # set default settings / load saved settings
        if self.master:
            self.settings = {
                "vcpu_count": None,
                "emulator_vcpus": 1,
                "allowed_vcpus": None,
                "cpu_package": None,
                "gdbinit": [],
                "gdbserver": [],
                "trace_events": None,
                "trace_limit": 0,
                "statefile": None,
                "tty": None
            }
            if settings:
                self.settings.update(settings)

            if self.settings["trace_events"] != None:
                if len(self.settings["trace_events"]) == 0:
                    self.settings["trace_events"].append("kvm")
        else:
            self.settings = {
                "tty": None
                }
            saved_settings = self.runconfig.get('settings')
            if saved_settings:
                self.settings.update(saved_settings)

        #########################################
                
        self.background = master
        self.foreground = True

        self.fsdir = self.vmdir + "/.vmfs"
        self.logdir = self.vmdir + "/.logs"
        self.debugdir = self.vmdir + "/.debug"
        self.fwdir = self.vmdir + "/.fw"

        self.socketdir = self.runconfig.get("socketdir", None)

        if self.socketdir == None:
            self.socketdir = "/run/vmspawn/" + self.vmname + "_" + lib.misc.random_string(8)
            self.runconfig.set("socketdir", self.socketdir)

        self.monitor_socket = self.socketdir + "/monitor.socket"
        self.hmp_socket = self.get_socketdir() + "/hmp_monitor.socket"
        self.qmp_socket = self.get_socketdir() + "/qmp_monitor.socket"
        self.migration_socket = self.get_socketdir() + "/migration.socket"

        # load configuration file
        self.debug("Loading the VM config file")
        os.environ["socketdir"] = self.socketdir
        os.environ["fwdir"] = self.fwdir
        self.config_manager = core.ConfigManager(self, self.vmdir + '/config.yaml')

        # create the device tree
        self.device_tree = self.config_manager.parse_device_tree()

        # add internal devices
        internal_devices = devices.Device(self, "internal_devices")
        internal_devices.add_subdevice(HmpMonitor(self, self.hmp_socket))
        internal_devices.add_subdevice(QmpMonitor(self, self.qmp_socket))
        internal_devices.add_subdevice(SerialPortDev(self, self.get_socketdir() + "/serial.socket",
                                              self.get_logdir() + "/serial.log",
                                              wait = self.settings["tty"] == True))
        internal_devices.add_subdevice(DebugCon(self, self.get_logdir() + "/firmware.log", 0x402))
        self.device_tree.add_subdevice(internal_devices)

        # init sub-daemons
        self.debug("Creating the daemons ")
        self.bkg = launchers.BackgroundLauncher(self)
        self.pr_helper = launchers.PrHelperLauncher(self)
        self.swtpm = launchers.SwTpmLauncher(self)
        self.qemu = launchers.QemuLauncher(self)
        self.daemons = (self.pr_helper, self.swtpm, self.qemu) #order matters here

        if extra_qemu_cmdline != None:
            for arg in extra_qemu_cmdline:
                self.qemu.add_emulator_arg(arg)
            
    ############################################################################
    # startup the VM

    def start(self, background = True):

        if not self.master:
            raise lib.InternalError()

        if self.get_running_status(verbose = True):
            if self.runconfig.get("running"):
                raise lib.UserError("VM is running on " + self.get_hostname() + ", please stop it first")
            else:
                raise lib.UserError("Some components of the VM are running, on " + self.get_hostname() + " please force stop the VM")

        self.info ("VM: starting " + self.get_vmname() + " on " + self.get_hostname())

        self.debug("Removing stale config files ")
        self._clean_stale_files(remove_logs = True)
        self._create_dirs()

        self.debug("Cleaning and saving runconfig ")
        self._clear_runconfig()
        self.runconfig.set("settings", self.settings)
        self.runconfig.save()

        try:
            self.__start(background)
        except:
            self.stop(force = True, quiet = True)
            raise

    ############################################################################

    def __start(self, background = True):

        #==========================================
        #fork into background
        if background:
            self.bkg.enable()
            self.bkg.launch()

            # and now wait for the background daemon to finish its job
            if not self.master:
                self.notice("Waiting for the background process to start its daemons")
                self.bkg.wait_startup()
                self.notice("Background daemon done starting")
                return

        self.runconfig.set("running", True)
        self.runconfig.set('hostname', socket.getfqdn())
        self.runconfig.set('last_hostname', socket.getfqdn())

        #==========================================
        # configure and startup the devices

        self.debug("Configuring devices")
        self.core_manager = core.CoreManager(self)
        self.device_tree.configure_pass1()
        self.device_tree.configure_pass2()
        self.core_manager.initCores(self.settings["allowed_vcpus"], self.settings["cpu_package"])

        self.notice("Starting up the devices...")
        if os.path.exists(self.get_vmdir() + "/.startup.sh"):
                subprocess.run(self.get_vmdir() + "/.startup.sh", check = True)
        self.device_tree.startup()

        #==========================================
        # Final debug prints
        self.debug("Final device tree:")
        if self.verbose > 2:
            self.device_tree.print()

        self.notice("Final core allocations:")
        if self.verbose > 1:
            self.core_manager.printInfo()

        self.notice("Saved run config")
        self.runconfig.save()

        #==========================================
        # launch & wait daemons
        self.notice("Waiting for daemons to initialize")
        for daemon in self.daemons:
            if daemon.is_enabled():
                daemon.launch()
                daemon.wait_startup()

                if not daemon.is_running():
                    raise lib.RuntimeError(daemon.log_crash())


        #==========================================
        # pin the qemu threads
        self.notice("Pinning all the threads...")

        while not self.core_manager.pinThreads():
            if not self.qemu.is_running():
                raise lib.RuntimeError(self.qemu.log_crash())
            time.sleep(0.1)

        #==========================================
        # load the saved state
        if self.settings["statefile"]:
            if not os.path.isfile(self.settings["statefile"]):
                raise lib.UserError("Save file " + self.settings["statefile"] + " doesn't exit")

            self.notice("Waiting for migration socket to appear")

            lib.misc.wait_file(self.migration_socket, 0)
            self.info("Loading saved state....")
            subprocess.run([
                'socat',
                'UNIX-CONNECT:' + self.migration_socket,
                'OPEN:' + self.settings["statefile"]
            ],
            check = True)
            self.notice("Save state load done")

        #==========================================
        # tell the world that we are launched
        if self.background:
            self.bkg.mark_launched()

        self.notice("All daemons initialized")

    ############################################################################
    # stop the VM
    def stop(self, force = False, quiet = False):

        if not self.runconfig.get("running") and not force:
            raise lib.UserError("VM isn't running")

        if not self.master and self.bkg.is_running() == False and not force:
            if not quiet:
                self.warn("Force stopping VM - background daemon not running")
            force = True
        elif force:
            if not quiet:
                self.info("Force stopping VM")
        else:
            if not quiet:
                self.info("Stopping VM.")

        # Stop the background daemon first
        if self.bkg.is_running() == True:
            self.bkg.stop(True)

        # Stopping all the daemons
        if force or self.master:
            for daemon in reversed(self.daemons):
                daemon.stop(force)

        # cleanup
        if self.master or force:
            self.notice("Tearing down the devices")
            self.device_tree.teardown()
            self.debug("Removing stale config files ")
            self._clean_stale_files()
            self._clear_runconfig()

            if os.path.exists(self.get_vmdir() + "/.shutdown"):
                subprocess.run(self.get_vmdir() + "/.shutdown")

    ############################################################################
    # get the VM running status
    def get_running_status(self, verbose = False):

        running = False
        if self.runconfig.get("running"):
            if verbose:
                self.info("VM is running on " + self.get_hostname())
            running = True

        for daemon in self.daemons:
            if daemon.is_running():
                if verbose:
                    self.notice(daemon.name + " IS running");
                running = True

        return running


    ############################################################################
    def _wait_startup(self):
        for daemon in self.daemons:
            if daemon.is_enabled():
                daemon.wait_startup()

                if not daemon.is_running():
                    raise lib.RuntimeError(daemon.log_crash())

    ############################################################################
    def get_vm_ip(self):

        mac = self.runconfig.get("mac_address", None)
        if mac == None:
            return None

        if self.verbose:
            print ("Found VM's mac: " + mac)
        mac = mac.lower()

        # HACK: try arp cache
        process = subprocess.run(['arp', '-n'], stdout = subprocess.PIPE)
        result = process.stdout.decode('utf-8')

        for line in result.split("\n"):
            if len(line) < 3:
                continue
            cols = line.split()
            if cols[2] == mac:
                return cols[0]

        #TODO: use qemu-ga to make the guest itsel tell its IP address for interface
        # which has the assigned mac address
        return None

    ############################################################################
    def get_hostname(self):
        return self.runconfig.get('hostname', socket.getfqdn())

    ############################################################################
    def warn(self, message):
        print("WARN : " + message)

    def info(self, message):
        print(message)

    def notice(self, message):
        if self.verbose > 0:
            print(message)

    def debug(self, message):
        if self.verbose > 1:
            print(message)

    def trace(self, message):
        if self.verbose > 9:
            print(message)

    def verbose_trace(self, message):
        if self.verbose > 99:
            print(message)


    ############################################################################
    def _create_dirs(self):
        if not os.path.exists(self.logdir):
            os.makedirs(self.logdir)
        if not os.path.exists(self.socketdir):
            os.makedirs(self.socketdir)
        if not os.path.exists(self.debugdir):
            os.makedirs(self.debugdir)
        if not os.path.exists(self.fwdir):
            os.makedirs(self.fwdir)

    ############################################################################
    def _remove_dir(self, dir, remove_files = False):
        if remove_files:
            for filename in glob.glob(dir + "/*"):
                os.remove(filename)
        try:
            os.rmdir(dir)
        except:
            pass

    ############################################################################
    def _clean_stale_files(self, remove_logs = False):
        self._remove_dir(self.get_socketdir(), True)
        self._remove_dir(self.get_debugdir(), True)

        for daemon in self.daemons:
            daemon.cleanup()

        if os.path.exists(self.get_fsdir()):
            self.notice("Unmounting the vm fs dir ")
            subprocess.run(["sudo", "umount", "-l", self.get_fsdir() + "/"])
            self._remove_dir(self.get_fsdir(), False)

        if remove_logs:
            self._remove_dir(self.get_logdir(), True)

    ############################################################################

    def _clear_runconfig(self):
        self.debug("Clearning the runconfig")
        self.runconfig.clear(["socketdir", "last_hostname", "vcpus_pin", "emulator_pin"])
        self.runconfig.save()

    ############################################################################
    def get_vmname(self): return self.vmname
    def get_vmdir(self): return self.vmdir
    def get_logdir(self): return self.logdir
    def get_socketdir(self): return self.socketdir
    def get_debugdir(self): return self.debugdir
    def get_fsdir(self): return self.fsdir
