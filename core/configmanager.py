import lib
import core
import devices
import os
import shutil

class ConfigManager():
    ############################################################################
    def __init__(self, vm, configfile):
        self.vm = vm
        self.configfile = configfile

        if os.path.isfile(configfile):
            self.vm_yaml_config = lib.load_yaml(configfile)
        else:
            raise lib.UserError(self.vm.vmdir + " doesn't appear to contain an VM")

        self.parse_environ()

    ############################################################################
    def _check_conditions(self, condition):

        #TODO: replace this hackjob with proper parser
        for or_part in condition.split("||"):
            or_part = or_part.strip();

            violated = False
            for and_part in or_part.split("&&"):
                and_part = and_part.strip()
                if  not self._check_condition(and_part):
                    violated = True
                    break

            if not violated:
                return True

        return False

    ############################################################################
    def _check_condition(self, condition):
        parts = None
        if "!=" in condition:
            positive = False
            parts = condition.partition("!=")
        elif "==" in condition:
            positive = True
            parts = condition.partition("==")
        else:
            envvar = condition[1:].strip()
            return os.getenv(envvar) != None


        if len(parts) != 3 or parts[0][0] != '$':
            raise lib.UserError("not supported/malformed conditon " + "_".join(parts))

        envvar = parts[0][1:].strip()
        value = parts[2].strip()
        envvarvalue = os.getenv(envvar);


        result = (envvarvalue != None and envvarvalue == value) or (envvarvalue == None and value == "@UNDEF@")

        if not positive:
            result = not result

        return result

    ############################################################################
    def _visit_config(self, callback, parent_obj, yaml_node, yaml_node_name, level = 0):
        # callback will be called on each node, and its result will be passed as 'parent'
        # to the child nodes

        leaf = not isinstance(yaml_node, dict)
        yaml_node_value = str(yaml_node) if leaf else None

        # check condition node in children
        if not leaf:
            for condition_node_name in ("condition", "+condition"):
                if condition_node_name in yaml_node and not self._check_conditions(yaml_node[condition_node_name]):
                    return


        # create this node
        this_node_obj = callback(parent_obj, yaml_node_name, yaml_node_value)
        if not this_node_obj:
            return this_node_obj

        # and descend into children
        if not leaf:
            for child_node_name, child_node in yaml_node.items():

                self.vm.verbose_trace("\t" * level + "testing node " + child_node_name)

                if child_node_name[0] == '-' and child_node_name[1] != '-':
                    continue
                if child_node_name[0] == "+":
                    child_node_name = child_node_name[1:]
                if child_node_name == "condition":
                    continue

                self.vm.verbose_trace("\t" * level + "visiting node " + child_node_name)
                self._visit_config(callback, this_node_obj, child_node, child_node_name, level + 1)

        return this_node_obj

    ############################################################################
    # parse env vars in the config file
    def parse_environ(self):
        def callback(parent_obj, yaml_node_name, yaml_node_value):
            if yaml_node_name.startswith("$"):
                env_var_name = yaml_node_name[1:]
                env_var_value = lib.misc.expand_basic(yaml_node_value)
                self.vm.debug("\t$" + env_var_name + "=" + env_var_value)
                os.environ[env_var_name] = env_var_value
            return True

        self.vm.debug("Setting ENV variables")
        self._visit_config(callback, None, self.vm_yaml_config, "root");

    ############################################################################
    # parse device tree from the config file
    def parse_device_tree(self):
        def callback(parent_obj, yaml_node_name, yaml_node_value):
            # filter non device nodes
            if yaml_node_name.startswith("$"):
                return

            # filter root node 'programs'
            if yaml_node_name == "programs" and parent_obj.parent == None:
                return False

            if yaml_node_value == None:
                # node with no qemu arguments, but probably with subdevices
                device = devices.Device(self.vm, name = yaml_node_name)
            else:
                arg_list = lib.misc.expand_multiline(yaml_node_value)
                for arg in arg_list:
                    self.vm.trace("\t" + arg)

                if len(arg_list) == 1:
                    # single arg
                    device = devices.QemuCmdDevice(self.vm, name = yaml_node_name, cmdline = arg_list[0])
                else:
                    # multiple arguments - create a container device for them
                    device = devices.Device(self.vm, name = yaml_node_name)
                    i = 0
                    for arg in arg_list:
                        sub_device = devices.QemuCmdDevice(self.vm, name = yaml_node_name + "." + str(i), cmdline = arg)
                        i = i + 1
                        device.add_subdevice(sub_device)

            if parent_obj:
                parent_obj.add_subdevice(device)
            return device

        self.vm.debug("Loading device tree")
        return self._visit_config(callback, None, self.vm_yaml_config, "root");

    ############################################################################
    # get config file by path
    def _get_config(self, path = None, default_value = None, tree = None):
        if tree == None:
            tree = self.vm_yaml_config

        if path == None or path == "/":
            return tree

        if isinstance(path, str):
            path = path.split("/")

        key = path[0]

        if not key in tree:
            return default_value

        value = tree[key]

        if len(path) > 1:
            if not type(value) is dict:
                return default_value
            return self._get_config(path[1:], default_value, value)

        if type(value) is str:
            value = value.strip()

        return value

    ############################################################################
    # get executable location

    def get_exec_path_and_args(self, config_name, exec_file_names):

        overriden_location = self._get_config("programs/" + config_name)

        if overriden_location:
            overriden_location = lib.misc.expand_multiline(overriden_location)
            overriden_location[0] = os.path.realpath(overriden_location[0])

            if not os.path.isfile(overriden_location[0]):
                self.vm.notice("" + config_name + " is not found at location specified in the config file " + location[0])
                return None

            self.vm.notice("Using " + overriden_location[0] + " for " + config_name + " from config file")
            return overriden_location

        for exec_name in exec_file_names:
            default_location = shutil.which(exec_name)

            if default_location:
                self.vm.notice("Using system default location " + default_location + " for program " + config_name)
                return [default_location]

        self.vm.notice("Program " + config_name + " was not found anywhere")
        return None
