import os
import psutil
import lib
import core

#######################################################################################################################################
# A single thread of some core
#######################################################################################################################################
class CpuThread():

    # create a new thread slot with given linux id, and initial allocation score
    def __init__(self, id, vm, score):
        assert(isinstance(vm, core.VM))
        self.id = id
        self.assigned = False
        self.owner = None
        self.expected_thread_name = None
        self.score = score
        self.vm = vm

    # allocate this thread to use
    def take(self, owner, expected_thread_name = None):
        self.owner = owner
        self.expected_thread_name = expected_thread_name
        self.assigned = True
        self.vm.trace("Allocated thread for " + str(owner) + " on core " + str(self.id))
        return self

    # get linux cpu number of this thread
    def get_id(self):
        return self.id

    # get score of this thread
    def get_score(self):
        return self.score

    # return if this thread is already assigned
    def is_assigned(self):
        return self.owner != None

    # return the owner of this thread
    def get_owner(self):
        return self.owner

    # return expected thread name of this thread
    def get_expected_thread_name(self):
        return self.expected_thread_name

    # print me
    def printInfo(self):
        thread_name = "other" if self.expected_thread_name == None else self.expected_thread_name
        self.vm.notice(" ** cpu" + str(self.id) + ":" + str(self.owner.name + "/" + thread_name))

#######################################################################################################################################
# A single core, consisting of one or two threads
#######################################################################################################################################

class CpuCore():

    # create an empty cpu core
    def __init__(self, core_id, vm):
        self._threads = {}
        self._core_id = core_id
        self.vm = vm
        pass

    def get_id(self):
        return self.core_id

    # add a new thread to this core
    def addThread(self, thread):
        self._threads[thread.get_id()] = thread;

    # get best score of one of the threads on this core
    def getScore(self):
        scores = []
        for _, thread in sorted(self._threads.items()):
            if not thread.is_assigned():
                scores.append(thread.get_score())
        if len(scores) == 0:
            return 0;
        else:
            return min(scores)

    # is this core not used by anyone
    def isFullyFree(self):
        for _, thread in sorted(self._threads.items()):
            if thread.is_assigned():
                return False
        return True

    # allocate one of the threads on this core
    def allocateThread(self, owner, expected_thread_name = None):
        for _, thread in sorted(self._threads.items()):
            if not thread.is_assigned():
                return thread.take(owner, expected_thread_name)
        return None

    def getAllocatedThreads(self, owner):
        retval = []
        for _, thread in sorted(self._threads.items()):
            if thread.is_assigned():
                if owner == None or thread.get_owner() == owner:
                    retval.append(thread)
        return retval

    def printInfo(self):
        self.vm.notice(" * Core " + str(self._core_id) + ": ")
        for _, thread in sorted(self._threads.items()):
            if thread.is_assigned():
                thread.printInfo()

#######################################################################################################################################
# A package made of some cores
#######################################################################################################################################


class CpuPackage():

    def __init__(self, package_id, vm):
        self._cores = {}
        self._package_id = package_id;
        self.vm = vm

    def get_id(self):
        return self._package_id

    # gets a core with given core id, or creates it
    def getCore(self, core_id, add = False):
        if core_id in self._cores:
            return self._cores[core_id]

        if not add:
            return None;

        core = CpuCore(core_id, self.vm);
        self._cores[core_id] = core;
        return core

    # allocates a thread on one of the cores
    def allocateThread(self, owner, exclusive, expected_thread_name):

        best_core = -1
        best_score = 0

        for _, core in sorted(self._cores.items()):
            if exclusive and not core.isFullyFree():
                continue

            score = core.getScore()
            if score > best_score:
                best_core = core
                best_score = score

        if best_score == 0:
            return None

        return best_core.allocateThread(owner, expected_thread_name)

    def getAllocatedThreads(self, owner):
        retval = []
        for _, core in sorted(self._cores.items()):
            retval += core.getAllocatedThreads(owner);
        return retval

    def printInfo(self):
        self.vm.notice("Package " + str(self._package_id) + ":")
        for _, core in sorted(self._cores.items()):
            core.printInfo()

#######################################################################################################################################
# Core manager
#######################################################################################################################################

class CoreManager():
    def __init__(self, vm):
        self._packages = {}
        self.vm = vm

    # late initialization of the core manager with allowed cores and default cpu package
    def initCores(self, allowed_logical_cores_str, default_package_id_str = None):

        cpu_devices_path = '/sys/bus/cpu/devices'
        allowed_logical_cores = None


        if allowed_logical_cores_str != None:
            allowed_logical_cores = {}

            for core_score in allowed_logical_cores_str.split(";"):
                if len(core_score) > 0:
                    core, score = core_score.split(":")
                    allowed_logical_cores[int(core)] = int(score)

        default_package_id = None
        if default_package_id_str:
            default_package_id = int(default_package_id)

        for logical_cpu_name in os.listdir(cpu_devices_path):

            topology_path = cpu_devices_path + "/" + logical_cpu_name + "/topology"
            logical_cpu_id = int(logical_cpu_name[3:])  # cpu name is in form cpuNN

            package_id = int(lib.read_file(topology_path + "/physical_package_id"))
            core_id = int(lib.read_file(topology_path + "/core_id"))

            core = self.getPackage(package_id, True).getCore(core_id, True);

            score = 100
            if allowed_logical_cores:
                if not logical_cpu_id in allowed_logical_cores:
                    continue;
                score = allowed_logical_cores[logical_cpu_id]

            core.addThread(CpuThread(logical_cpu_id, self.vm, score))

        if default_package_id != None:
            self.default_package_id = default_package_id
            self.default_package = self.getPackage(default_package_id);
            if self.default_package == None:
                raise lib.UserError("Package " + str(default_package_id) + " doesn't exit")
        else:
            self.default_package = next(iter(self._packages.values()))
            self.default_package_id = None

    def getDefaultPackageId(self):
        return self.default_package_id

    # gets a package with given package ID
    def getPackage(self, package_id, add = False):

        if package_id in self._packages:
            return self._packages[package_id]

        if not add:
            return None;

        package = CpuPackage(package_id, self.vm);
        self._packages[package_id] = package;
        return package

    # this allocates an cpu from default package from first unused core
    # exclusive_priority is value between 0 and 100 which enforces on how likely this thread will to share a cpu with another one using HT
    def allocateThread(self, owner, exclusive, expected_thread_name = None):

        # first try on default package
        thread = self.default_package.allocateThread(owner, exclusive, expected_thread_name)
        if thread:
            return thread

        # then try all other packages
        for _, package in sorted(self._packages.items()):
            if package != self.default_package:
                thread = package.allocateThread(owner, exclusive, expected_thread_name)
                if thread:
                        return thread

        raise lib.NoResouceLeft("Can't allocate a thread")

    # return list of all thread ids that are allocated to someone
    def getAllocatedThreads(self, owner = None):
        result = []
        for _, package in sorted(self._packages.items()):
            result += package.getAllocatedThreads(owner)
        return result

    # print debug info
    def printInfo(self):
        self.vm.notice("Allocated cores:")
        self.vm.notice("NUMA package:" + str(self.default_package.get_id()))

        for _, package in sorted(self._packages.items()):
            package.printInfo()

    # pin all allocated threads
    def pinThreads(self):
        for log_core in self.getAllocatedThreads():
            owner = log_core.get_owner()
            cpu = log_core.get_id()

            threadname = log_core.get_expected_thread_name()
            if threadname == None:
                continue

            pid = owner.runconfig.get('pid')
            if not pid:
                continue

            if not psutil.pid_exists(pid):
                return False

            for tmp in psutil.Process(pid).threads():
                thread = psutil.Process(tmp.id)
                if thread.name() == threadname:

                    self.vm.debug("Pinning " + owner.name + " thread (pid=" + str(thread.pid) + ") with name '" + threadname + "' to logical cpu " + str(cpu))
                    thread.cpu_affinity([cpu])
                    break
            else:
                return False
        return True
