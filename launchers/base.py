import os
import signal
import time
import psutil
import lib
import core

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# This class represents base implementation of an launcher
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class Launcher:
    ####################################################################################################################
    # create a new abstract launcher given a VM configuration
    def __init__(self, vm, name):
        assert(isinstance(vm, core.VM))
        self.vm = vm
        self.name = name
        self.process = None
        self.pid = None
        self.exe = None
        self.exe_args = None
        self.childpid = None
        self.enabled = False
        self.refresh()

    ####################################################################################################################
    def set_exe(self, exe_and_args):
        self.exe = exe_and_args[0]
        self.exe_args = exe_and_args[1:]

    ####################################################################################################################

    def enable(self):
        if not self.is_configured():
            raise lib.UserError(self.name + " is needed for this run but it is not configured")
        self.enabled = True

    ####################################################################################################################
    # return true if this daemon can be launched
    def is_configured(self):
        return self.exe != None

    # check if this launcher is enabled
    def is_enabled(self):
        return self.enabled

    ####################################################################################################################

    # returns true if this daemon is running
    # MIGHT be false for a momement after startup if run from non master process, since pid file might not be created
    def is_running(self):

        if not self.vm.master:
            self.refresh()

        #  check if the daemon pid exists
        if self.pid != None and not psutil.pid_exists(self.pid):
            self.pid = None

        # check if the process we started exist (it can be debugger/tracer/etc)
        if self.process != None and self.process.poll() != None:
            self.process = None

        return self.process != None or self.pid != None

    ####################################################################################################################
    # launch the program
    # NOTE: doesn't wait for the startup
    def launch(self):

        self.vm.notice("Launching " + self.name)

        if self.is_running():
            return

        assert(self.vm.master)
        self.do_launch()

    def do_launch(self):
        raise lib.NotImplemented()

     ####################################################################################################################
    # wait till daemon is started
    # usually only master process should wait for its children
    def wait_startup(self):
        self.vm.notice("Waiting for " + self.name + " startup")

        self.do_wait_startup()

        if self.vm.master and self.pid:
            self.runconfig.set('pid', self.pid)
            self.runconfig.save()

        if not self.is_running():
            raise lib.RuntimeError(self.log_crash())

    def do_wait_startup():
        raise lib.NotImplemented()

    ####################################################################################################################
    # stop the daemon and wait for the stop
    def stop(self, force):

        if not self.is_running():
            return

        if not self.vm.master:
            if not force:
                self.vm.notice("Skipping " + self.name)
                return
            else:
                self.refresh()

        self.vm.notice("Stopping " + self.name)

        # normal python subprocess
        if self.process:
            self.process.send_signal(signal.SIGTERM)

        # forked child process
        if self.childpid:
            try:
                os.kill(self.childpid, signal.SIGTERM)
            except ProcessLookupError:
                self.childpid = None
                pass

        # just pid we got from text file
        if (self.pid and force):
                try:
                    os.kill(self.pid, signal.SIGTERM)
                except ProcessLookupError:
                    self.pid = None
                    pass

        self.wait_end()

   ####################################################################################################################
    # wait till daemon ends
    def wait_end(self):

        self.vm.info("Waiting for " + self.name + " to end")

        if not self.is_running():
            return

        if not self.vm.master:
            self.refresh()

        if self.process:
            self.process.wait()
            self.process = None

        if self.pid:
            while self.pid in psutil.pids():
                time.sleep(0.5)
                self.refresh()
            self.pid = None

    ####################################################################################################################
    def log_crash(self):
        message = self.name + " process crashed\n";
        if os.path.isfile(self.logfile):
            message += "Log:\n"
            message += lib.read_file_ident(self.logfile, "    ")
        return message.strip()

    ####################################################################################################################
    # cleanup stale files
    def cleanup(self):
        pass

    ####################################################################################################################
    def refresh(self):
        self.runconfig = lib.FileDict(self.vm.get_socketdir() + "/" + self.name + ".yaml")

        new_pid = self.runconfig.get('pid')
        if new_pid:
            if self.pid and new_pid != self.pid:
                print("WARNING: pid mismatch for process " + name)
            else:
                self.pid = new_pid

        else:
            if self.pid:
                self.pid = None
