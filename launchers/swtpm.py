import os
import subprocess

import launchers
import devices
import lib


# use with pr-manager=qemu_pr_helper on -drive/blockdef of a raw block device
class SwTpmLauncher(launchers.Launcher):
    ####################################################################################################################
    def __init__(self, vm):
        super().__init__(vm, "swtpm")
        self.logfile = self.vm.get_logdir() + "/swtpm.log"

        tmp = self.vm.config_manager.get_exec_path_and_args("swtpm", ["swtpm"])
        if tmp:
            self.set_exe(tmp)
            self.pidfile = self.vm.get_socketdir() + "/swtpm.pid"
            self.pid = self.runconfig.get('pid')

    ####################################################################################################################
    def do_launch(self):
        assert(self.vm.master)

        args = []

        args += [self.exe, "socket",
                    "--tpmstate", "dir=" + self.state_dir,
                    "--ctrl", "type=unixio,path=" + self.exe_socket,
                    "--pid", "file=" + self.pidfile,
                    "--tpm2",
                    "--log", "level=20",
                ] + self.exe_args

        if self.vm.verbose > 9:
            print(args)

        logfile = open(self.logfile, 'w')
        self.process = subprocess.Popen(args, stdout = logfile, stderr = logfile, start_new_session = True)


    ####################################################################################################################
    def do_wait_startup(self):
        assert(self.vm.master)

        while not lib.wait_file(self.pidfile, 1):
            if not self.is_running():
                return;

        while not lib.wait_file(self.exe_socket, 1):
            if not self.is_running():
                return;

        self.pid = int(lib.read_file(self.pidfile))

        if self.vm.master:
            self.runconfig.set('pid', self.pid)
            self.runconfig.save()

    ####################################################################################################################
    def enable(self, socket, swtpmdir):
        if self.is_enabled():
            raise lib.UserError("There can be only one swtpm defined")

        self.exe_socket = socket
        self.state_dir = swtpmdir
        super().enable()
