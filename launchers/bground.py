import launchers
import lib
import os
import time
import signal
import setproctitle


def handle_exit(sig, frame):
    raise(SystemExit)

class BackgroundLauncher(launchers.Launcher):
    ####################################################################################################################
    def __init__(self, vm):
        super().__init__(vm, "background")
        self.logfile = self.vm.get_logdir() + "/background.log"

    ####################################################################################################################
    def is_configured(self):
        return True

    ####################################################################################################################
    def do_launch(self):
        self.childpid = lib.daemonize(self.logfile);

        if self.childpid != 0:
            self.vm.notice("Doing the rest of the vm startup in the background...")
            self.child = True
            self.vm.background = False
            # we are no longer the VM master
            self.vm.master = False
        else:
            print("Background daemon started")
            signal.signal(signal.SIGTERM, handle_exit)
            self.vm.foreground = False
            setproctitle.setproctitle("vmstart_background")

    ####################################################################################################################
    def do_wait_startup(self):
        while self.pid == None:

            if self.childpid != None:
                if os.waitpid(self.childpid, os.WNOHANG) != (0, 0):
                    self.childpid = None
                    return

            self.refresh()
            time.sleep(0.1)


    ####################################################################################################################
    # as a special case we allow to stop this daemon when we are not master
    # as a means to stop a background VM
    def stop(self, force):
        return super().stop(True)


    ####################################################################################################################
    # used in no-background daemon mode, pretent that we are the daemon for external users
    def mark_launched(self):
        self.runconfig.set('pid', os.getpid())
        self.runconfig.save()
