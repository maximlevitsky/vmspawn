import os
import subprocess

import launchers
import devices
import lib


# use with pr-manager=qemu_pr_helper on -drive/blockdef of a raw block device
class PrHelperLauncher(launchers.Launcher):
    ####################################################################################################################
    def __init__(self, vm):
        super().__init__(vm, "pr_helper")
        self.logfile = self.vm.get_logdir() + "/pr_helper.log"

        tmp = self.vm.config_manager.get_exec_path_and_args("pr_helper", ["qemu-pr-helper"])
        if tmp:
            self.set_exe(tmp)
            self.exe_socket = self.vm.get_socketdir() + "/pr_helper.socket"
            self.pid = self.runconfig.get('pid')

    ####################################################################################################################
    def do_launch(self):
        assert(self.vm.master)

        args = []
        if "pr_helper" in self.vm.settings["gdbserver"]:
            args = ["gdbserver", ":12345" ]

        args += [self.exe, *self.exe_args, '-k', self.exe_socket, '-f', self.vm.get_socketdir() + "/pr_helper.pid"]

        logfile = open(self.logfile, 'w')
        self.process = subprocess.Popen(args, stdout = logfile, stderr = logfile, start_new_session = True)

        if "pr_helper" in self.vm.settings["gdbserver"]:
            self.vm.info("Pr-helper's gdbserver is running on :12345")
            self.vm.info("Example GDB command line to attach to it:")
            self.vm.info("gdb --ex 'target remote :12345' --ex 'continue'")

    ####################################################################################################################
    def do_wait_startup(self):
        assert(self.vm.master)

        while not lib.wait_file(self.vm.get_socketdir() + "/pr_helper.pid", 1):
            if not self.is_running():
                return;

        while not lib.wait_file(self.vm.get_socketdir() + "/pr_helper.socket", 1):
            if not self.is_running():
                return;

        self.pid = int(lib.read_file(self.vm.get_socketdir() + "/pr_helper.pid"))

        if self.vm.master:
            self.runconfig.set('pid', self.pid)
            self.runconfig.save()

    ####################################################################################################################
    def enable(self):
        if not self.is_enabled():
            super().enable()
            self.vm.qemu.add_emulator_arg("-object pr-manager-helper,id=qemu_pr_helper,path=" + self.exe_socket)
