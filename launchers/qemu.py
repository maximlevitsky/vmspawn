import os
import subprocess
import launchers
import textwrap
import lib
import time
import signal

class QemuLauncher(launchers.Launcher):
    ####################################################################################################################
    def __init__(self, vm):
        super().__init__(vm, "qemu")

        tmp = self.vm.config_manager.get_exec_path_and_args("qemu",
                    ["qemu-system-x86_64", "/usr/libexec/qemu-kvm", "qemu-kvm"])

        self.logfile = self.vm.get_logdir() + "/qemu.log"
        self.pass_fds = []

        if tmp:
            self.set_exe(tmp)
            self.pid = self.runconfig.get('pid')
        else:
            raise lib.UserError("Qemu is needed for the VM run")

        self.enable()
        self.suspend_inhibit = False

    ####################################################################################################################
    def _launch_external(self, raw_args):
        argsstr = ' \\\n\t'.join(raw_args)

        if (len(self.pass_fds)):
            raise lib.UserError("Can run external gdb with passed FD's")

        if self.vm.settings["trace_events"] != None:
            raise lib.UserError("Can't kvm trace with external gdb")

        with open(self.vm.get_debugdir() + "/qemu.gdbinit", "w") as gdbinitfd:
            gdbinitfd.write(textwrap.dedent("""\
                # SIGUSR1 is used in qemu as cross-thread wakeup signal
                handle SIGUSR1 noprint
                handle SIGTERM noprint
                set cwd """ + self.vm.get_vmdir() + """
                set remote exec-file """ + self.exe + """
                exec-file target:""" + self.exe + """
                symbol-file target:""" + self.exe + """
                """))
            gdbinitfd.write("set args " + argsstr)
            gdbinitfd.write("\n\n")

        os.symlink(self.exe, self.vm.get_debugdir() + "/qemu")
        self.vm.info("QEMU init files for QEMU are created. Waiting for you to start QEMU from your debugger...")
        self.vm.info("Example gdb start command: (in VM folder):")
        self.vm.info("$ sudo gdb -x ./.debug/qemu.gdbinit ./.debug/qemu -ex 'run' ")

    ####################################################################################################################
    def _launch_internal(self, raw_args):

        launch_args = []

        if self.suspend_inhibit:
            launch_args += ["systemd-inhibit",
                            '--what=sleep:shutdown',
                            "--mode=block",
                            "--who=" + self.vm.get_vmname(),
                            "--why=VM with VFIO devices"
                            ]

        launch_args += ["taskset", "-c", ",".join(self.vm.runconfig.get("emulator_pin"))]

        if self.vm.core_manager.getDefaultPackageId() != None:
            launch_args += ["numactl", "-N", str(self.vm.core_manager.getDefaultPackageId()), "-m", str(self.vm.core_manager.getDefaultPackageId())]

        if self.vm.settings["trace_events"] != None:
            launch_args += ["trace-cmd", "record"]
            self.vm.notice("Starting trace-cmd -e " + ",".join(self.vm.settings["trace_events"]) + "-o /tmp/trace.dat")
            self.vm.info("After VM ends running, to view the trace, use")
            self.vm.info("$ vm trace --report")

            for event in self.vm.settings["trace_events"]:
                launch_args.append("-e" + event)

            if self.vm.settings["trace_limit"]:
                limit = int(self.vm.settings["trace_limit"])
                launch_args += ["-m", str(limit * 1024)]

            launch_args += ["-o" + "/tmp/trace.dat" ]

        elif "qemu" in self.vm.settings["gdbserver"]:
            launch_args += ["gdbserver", ":1234", "--"]

        launch_args.append(self.exe)
        launch_args += raw_args

        if self.vm.verbose > 9:
            print(launch_args)

        qemu_log_file = open(self.logfile, 'w')
        self.process = subprocess.Popen(launch_args, cwd = self.vm.get_vmdir(),
                                        stdout = qemu_log_file, stderr = qemu_log_file,
                                        start_new_session = True, pass_fds = self.pass_fds)

        if "qemu" in self.vm.settings["gdbserver"]:
            self.vm.info("Qemu's gdbserver is running on :1234")
            self.vm.info("Example GDB command line to attach to it:")
            self.vm.info("gdb --ex 'target remote :1234' --ex 'continue'")

    ####################################################################################################################
    def do_launch(self):
        assert(self.is_configured())
        assert(self.vm.master)

        if "guest" in self.vm.settings["gdbserver"]:
            self.add_emulator_arg("-gdb tcp::2345")

        self.add_emulator_arg("-pidfile " + self.vm.get_socketdir() + "/qemu.pid")

        if self.vm.settings["statefile"] != None:
            self.add_emulator_arg("-incoming unix:" + self.vm.migration_socket)

        cmdline = self.vm.device_tree.cmdline()

        with open(self.vm.get_logdir() + "/cmdline.log", "w") as output:
            output.write(self.exe + "\n")

            for arg in self.exe_args:
                output.write(arg + "\n")

            for cmd in cmdline:
                output.write(cmd + "\n")

        args = []
        for arg in self.exe_args:
            args += arg.split()

        args += cmdline

        args += ["-name debug-threads=on"]

        raw_args = []
        for arg in args:
             split_arg = arg.split(maxsplit = 1)
             raw_args.append(split_arg[0])
             if len(split_arg) > 1:
                 arg = split_arg[1];
                 arg = arg.replace("%%dblcomma%%", ",,")
                 arg = arg.replace("%%eq%%", "=")
                 raw_args.append(arg)

        if "qemu" in self.vm.settings["gdbinit"]:
            self._launch_external(raw_args)
        else:
            self._launch_internal(raw_args)

    ####################################################################################################################
    # stop the daemon (qemu version)
    def stop(self, force):
        # HACK: seems like trace-cmd forks many processes to the background,
        # and that killing it doesn't stop neither them nor qemu
        if self.vm.master and self.vm.settings["trace_events"]:

            self.vm.debug("Special hack for stopping trace qemu")

            if self.pid:
                self.vm.notice("Killing trace-cmd process with SIGTERM and waiting for it to end")
                os.kill(self.pid, signal.SIGTERM)
                self.wait_end()
                self.vm.notice("trace-cmd done")
        else:
            return super().stop(force)

    def kill(self):
        if self.pid:
            os.kill(self.pid, signal.SIGKILL)
        else:
            self.vm.warn("NO qemu pid to kill")

    ####################################################################################################################
    def do_wait_startup(self):
        assert(self.vm.master)

        while not lib.wait_file(self.vm.get_socketdir() + "/qemu.pid", 1):
            if not "qemu" in self.vm.settings["gdbinit"]:
                if not self.is_running():
                    return;

        self.pid = int(lib.read_file(self.vm.get_socketdir() + "/qemu.pid"))

        while not lib.wait_file(self.vm.qmp_socket, 1) or not lib.wait_file(self.vm.hmp_socket, 1):
            if not self.is_running():
                return;

        if "guest" in self.vm.settings["gdbserver"]:
            self.vm.notice("qemu kernel gdbstub is running on :2345")

    ####################################################################################################################
    def add_emulator_arg(self, command):
        self.exe_args.append(command)

    def add_fd(self, fd):
        self.pass_fds.append(fd)

    def enable_suspend_inhibit(self):
        self.suspend_inhibit = True
