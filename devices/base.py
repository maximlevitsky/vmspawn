
import lib
import devices
import core

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# This class represents an device that consists of several sub devices, and optionally device itself
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class Device():
    def __init__(self, vm, name):
        assert(isinstance(vm, core.VM))
        self.vm = vm
        self._subdevices = []
        self._name = name
        self._type = None
        self._id = None
        self.parent = None

    ##########################################################################################################
    # gets internal device name
    def get_name(self):
        return self._name
    
    def get_id(self):
        return self._id

    ##########################################################################################################
    # adds a subdevice to this device
    def add_subdevice(self, subdevice):

        assert(isinstance(subdevice, Device))

        subdevice.parent = self
        self._subdevices.append(subdevice)

    ##########################################################################################################
    # return all subdevices
    def subdevices(self):
        result = []

        for device in self._subdevices:
            result.append(device)
            result += device.subdevices()

        return result

    ##########################################################################################################
    # do configuration steps that might change this device settings, add new devices, etc
    # however no system wide changes should be made
    def configure_pass1(self):
        for device in self._subdevices:
            device.configure_pass1()

    def configure_pass2(self):
        for device in self._subdevices:
            device.configure_pass2()

    ##########################################################################################################
    # startup the device, might need system wide changes, like attaching vfio
    # doesn't change the device tree
    def startup(self):
        for device in self._subdevices:
            device.startup()

    ##########################################################################################################
    # teardown the device, might also need system wide changes like detaching from vfio
    def teardown(self):
        for device in self._subdevices:
            device.teardown()

    ##########################################################################################################
    # returns list of qemu args that this device adds
    def cmdline(self):
        retval = []
        for device in self._subdevices:
            retval += device.cmdline()
        return retval

    ##########################################################################################################
    def print(self, level = 0):

        self.do_print(level)

        for device in self._subdevices:
            device.print(level + 1)
    ##########################################################################################################
    def do_print(self, level):
        print ("\t|" * level + self._name + ":")
