import lib
import devices
import subprocess
import re
import os
import os.path
import shutil
from lib.qoption import QOption

idcnt = 0

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# This class represents a device that is represented by a single qemu argument
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class QemuCmdDevice(devices.Device):

    ##############################################################################################################
    def __init__(self, vm, name, cmdline):
        super().__init__(vm, name)
        self._qoption = QOption(cmdline)
        self._set_id()
        self.tap_file_fd = None
        self.ifname = None

    ##############################################################################################################
    def configure_pass1(self):

        # configure number of vCPUs
        if self._qoption.get_option() == "smp" and self.vm.master:
            self.vm.settings["vcpu_count"] = int(self._qoption.get_arg("cpus", 1))
            self.vm.settings["allowed_vcpus"] = self._qoption.get_arg("~allowed_vcpus", None)
            self.vm.settings["emulator_vcpus"] = self._qoption.get_arg("~emulator_vcpus", 1)

        # enable pr-helper daemon if needed
        if self._qoption.has_arg("pr-manager"):
            pr_helper = self._qoption.get_arg("pr-manager")
            if pr_helper == "qemu_pr_helper":
                self.vm.pr_helper.enable()

        # enable swtpm daemon if needed
        if self._qoption.has_arg("~swtpmdir"):
            swtpmdir = self._qoption.get_arg("~swtpmdir")
            socket = self._qoption.get_arg("path")
            self.vm.swtpm.enable(socket, swtpmdir)

        if self._qoption.has_arg("~default_filename") and self._qoption.has_arg("filename"):
            filename = self._qoption.get_arg("filename", None)
            default_filename = self._qoption.get_arg("~default_filename", None)

            if not os.path.exists(filename):
                if os.path.exists(default_filename):
                    print("Copying default template from " + default_filename + " to " + filename)
                    shutil.copy(default_filename, filename);

        # store mac address of net interface
        # assumption: only one nic per VM for now
        if self._qoption.get_option() == "device" and self._qoption.has_arg("mac"):
            self.vm.runconfig.set("mac_address", self._qoption.get_arg("mac"))

        super().configure_pass1()

    ##############################################################################################################
    def configure_pass2(self):
        # set default number of queues for virtio-devices
        if self.vm.settings["vcpu_count"] and self._qoption.get_option() == "device":
            vcpu_count_str = str(self.vm.settings["vcpu_count"])
            if self._qoption.get_arg("type") == "virtio-scsi":
                self._qoption.set_arg("num_queues", vcpu_count_str, override = False)
            if self._qoption.get_arg("type") == "virtio-blk":
                self._qoption.set_arg("num-queues", vcpu_count_str, override = False)

        super().configure_pass2()

    ##############################################################################################################
    def startup(self):
        # allocate vCPU and emulator threads
        if self._qoption.get_option() == "smp":
            vm_cpus = []
            emulator_cpus = []

            smp = self._qoption.has_arg("threads") and int(self._qoption.get_arg("threads")) == 2

            for i in range(0, self.vm.settings["vcpu_count"]):
                thread = self.vm.core_manager.allocateThread(owner = self.vm.qemu, exclusive = not smp, expected_thread_name = "CPU " + str(i) + "/KVM")
                vm_cpus.append(thread.get_id())

            for i in range(0, int(self.vm.settings["emulator_vcpus"])):
                thread = self.vm.core_manager.allocateThread(owner = self.vm.qemu, exclusive = False)
                emulator_cpus.append(str(thread.get_id()))

            self.vm.runconfig.set("vcpus_pin", vm_cpus)
            self.vm.runconfig.set("emulator_pin", emulator_cpus)

        # for iothread 'devices' allocate the device thread
        if self._qoption.get_option() == "object" and self._qoption.get_arg("type") == "iothread":
            self.vm.core_manager.allocateThread(owner = self.vm.qemu, exclusive = False, expected_thread_name = "IO " + self._qoption.get_arg("id"))

        # attach vfio devices
        if self._qoption.get_option() == "device" and self._qoption.get_arg("type") == "vfio-pci" and self._qoption.has_arg("host"):

            self.vm.qemu.enable_suspend_inhibit()

            pci_dev = self._qoption.get_arg("host")
            if len(pci_dev.split(":")) == 2:
                pci_dev = "0000:" + pci_dev
            subprocess.run(["vfio_adm", "--attach", "--device", pci_dev ], check = True)

        # attach network tap devices to bridge/macvtap
        if self._qoption.get_option() == "netdev" and self._qoption.get_arg("type") == "tap":

            if self._qoption.has_arg("~bridge"):
                self.ifname = self._qoption.get_arg("ifname")

                if not self.ifname:
                    raise lib.UserError("TAP bridge device must have ifname=");

                for bridge in self._qoption.get_arg("~bridge").split("|"):
                    if not os.path.exists("/sys/class/net/" + bridge):
                        continue

                    print("Using bridge " + bridge + " for " + self.ifname)

                    subprocess.run("ip tuntap add dev " + self.ifname + " mod tap", shell = True, check = True)
                    subprocess.run("ip link set " + self.ifname + " master " + bridge + " up", shell = True, check = True)
                    self._qoption.set_arg("script", "no", override = True)
                    self._qoption.set_arg("downscript", "no", override = True)
                    break
                else:
                    raise lib.UserError("none of bridges for " + self.ifname + " exist")

            elif self._qoption.has_arg("~macvtap"):
                self.ifname = self._qoption.get_arg("~ifname")
                (self.hw_iface, self.tap_mac) = self._qoption.get_arg("~macvtap").split(":", 1)

                if not self.ifname:
                    raise lib.UserError("TAP MACVTAP device must have ~ifname");

                subprocess.run("ip link add link " + self.hw_iface + " name " + self.ifname + " type macvtap", shell = True, check = True)
                subprocess.run("ip link set " + self.ifname + " address " + self.tap_mac + " up", shell = True, check = True)
                self.tap_filename = "/dev/tap" + str(int(lib.misc.read_file("/sys/class/net/" + self.ifname + "/ifindex")))

                self.tap_file_fd = os.open(self.tap_filename, os.O_RDWR)
                self._qoption.set_arg("fd", str(self.tap_file_fd), override = True)
                self.vm.qemu.add_fd(self.tap_file_fd)


        super().startup()

    ##############################################################################################################
    def teardown(self):
        super().teardown()

        # detach vfio devices
        if self._qoption.get_option() == "device" and self._qoption.get_arg("type") == "vfio-pci" and self._qoption.has_arg("host"):
            pci_dev = self._qoption.get_arg("host")
            if len(pci_dev.split(":")) == 2:
                pci_dev = "0000:" + pci_dev
            subprocess.run(["vfio_adm", "--detach", "--device", pci_dev ], check = False)

        # detach network tap devices
        if self._qoption.get_option() == "netdev" and self._qoption.get_arg("type") == "tap":
            if self.tap_file_fd:
                try:
                    os.close(self.tap_file_fd)
                except:
                    pass

            if self._qoption.has_arg("~bridge"):
                subprocess.run("ip link delete " + self._qoption.get_arg("ifname"), shell = True, check = False,
                               stderr = subprocess.DEVNULL, stdout = subprocess.DEVNULL)
            elif self._qoption.has_arg("~macvtap"):
                subprocess.run("ip link delete " + self._qoption.get_arg("~ifname"), shell = True, check = False,
                               stderr = subprocess.DEVNULL, stdout = subprocess.DEVNULL)


    ##############################################################################################################

    def cmdline(self):
        return [self._qoption.construct_cmdline()] + super().cmdline()

    ##############################################################################################################

    def do_print(self, level):
        print ("\t|" * level + self._name + ": " + self._qoption.construct_cmdline())

    ##############################################################################################################
    def _set_id(self):
        global idcnt

        if self._qoption.has_arg("id"):
            self._id = self._qoption.get_arg("id")
        else:
            self._id = "auto_id_" + str(idcnt)
            idcnt = idcnt + 1
            if self._qoption.get_option() == "device":
                self._qoption.set_arg("id", self._id, override = False)
