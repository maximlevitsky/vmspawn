#! /bin/bash

BASE=$(dirname "$BASH_SOURCE")
BASE=$(realpath $BASE)

export PATH=$BASE:$PATH

_vm_completion()
{

  if [ ! "$COMP_CWORD" = "1" ] ; then
    local cur=${COMP_WORDS[COMP_CWORD]}
    COMPREPLY=( $(compgen -f -- ${cur}) )
    return
  fi

  words="$(ls $BASE/cmds) hssh "

  if [ -e .scripts ] ; then
    words+="$(ls .scripts/ --hide '_*' ) "
  fi
  
  if [ -e ./.vmlink ] && [ -e $(cat ./.vmlink)/.scripts ]  ; then
    words+="$(ls $(cat ./.vmlink)/.scripts --hide '_*' ) "
  fi
  
  if [ -e $HOME/.vmspawn/scripts ] ; then
    words+="$(ls $HOME/.vmspawn/scripts --hide '_*' ) "
  fi

  COMPREPLY=($(compgen -W "$words" -- "${COMP_WORDS[1]}"))
}

complete -o filenames -F _vm_completion vm
